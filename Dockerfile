FROM docker:latest

RUN apk add --no-cache \
              curl \
              bash \
              make \
              ca-certificates \
              openssl \
              python3
RUN update-ca-certificates
# Download and install Google Cloud SDK
RUN wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz && \
    tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true && \
    google-cloud-sdk/bin/gcloud --quiet components update
